const { server, app } = require("./generateServer")();
const middlewares = require("./middlewares");
const rabbitmqService = require("@videowiki/workers/vendors/rabbitmq");
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
const videowikiGenerators = require('@videowiki/generators');

const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "/tmp");
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname +
        "-" +
        Date.now() +
        "." +
        file.originalname.split(".").pop()
    );
  },
});
const upload = multer({ storage: storage });
rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
  if (err) {
    throw err;
  }
  channel.on("error", (err) => {
    console.log("RABBITMQ ERROR", err);
    process.exit(1);
  });
  channel.on("close", () => {
    console.log("RABBITMQ CLOSE");
    process.exit(1);
  });
  require("./rabbitmqHandlers").init(channel);
  const workers = require('./workers')({ rabbitmqChannel: channel });

  videowikiGenerators.healthcheckRouteGenerator({ router: app, rabbitmqConnection: channel.connection })

  const controller = require("./controller")({ workers });
  
  app.all("*", (req, res, next) => {
    if (req.headers["vw-user-data"]) {
      try {
        const user = JSON.parse(req.headers["vw-user-data"]);
        req.user = user;
      } catch (e) {
        console.log(e);
      }
    }
    next();
  });

  app.post(
    "/:articleId/picInPic",
    upload.single("file"),
    middlewares.authorizeTranslationUpdate,
    controller.addPictureInPicture
  );
  app.patch(
    "/:articleId/picInPic/position",
    middlewares.authorizeTranslationUpdate,
    controller.updatePictureInPicturePosition
  );

  app.put(
    "/:articleId/stage/text_translation_done",
    controller.setStageToTextTranslationDone
  );
  app.put(
    "/:articleId/stage/voice_over_translation",
    controller.setStageToVoiceoverTranslation
  );
  app.put(
    "/:articleId/stage/voice_over_translation_done",
    controller.setStageToVoiceoverTranslationDone
  );
  app.put("/:articleId/stage/done", controller.setStageToDone);

  app.post('/:articleId/time', middlewares.authorizeTranslationUpdate, controller.updateSubslideTiming)
  app.post(
    "/:articleId/text",
    middlewares.authorizeTranslationUpdate,
    controller.addTranslatedText
  );
  app.post(
    "/:articleId/text/replace",
    middlewares.authorizeTranslationUpdate,
    controller.replaceTranslatedText
  );

  app.post(
    "/:articleId/audio",
    upload.single("file"),
    middlewares.authorizeTranslationUpdate,
    controller.addRecordedAudio
  );
  app.post(
    "/:articleId/audio/tts",
    middlewares.authorizeTranslationUpdate,
    controller.generateTTSAudio
  );
  app.post(
    "/:articleId/audio/original",
    middlewares.authorizeTranslationUpdate,
    controller.updateAudioFromOriginal
  );

  app.delete(
    "/:articleId/audio",
    middlewares.authorizeTranslationUpdate,
    controller.deleteRecordedAudio
  );

  app.post("/:articleId/videoSpeed", controller.updateVideoSpeed);
  app.post("/:articleId/audioSpeed", controller.updateAudioSpeed);
  app.post("/:articleId", controller.generateTranslatableArticle);

  app.post(
    "/:articleId/translationVersions/setTranslationVersionForAllSubslides",
    controller.setTranslationVersionForAllSubslides
  );
  app.post(
    "/:articleId/translationVersions/setTranslationVersionForSubslide",
    controller.setTranslationVersionForSubslide
  );
  app.get("/:articleId/translationVersions", controller.getTranslationVersion);
  app.get(
    "/:articleId/translationVersions/count",
    controller.getTranslationVersionCount
  );

  app.get("/:articleId/languages", controller.getTranslatableArticleLangs);
  // TODO: DOC THIS
  app.get("/:articleId", controller.getTranslatableArticle);
});

const PORT = process.env.PORT || 4000;
server.listen(PORT);
console.log(`Magic happens on port ${PORT}`); // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`);
exports = module.exports = app; // expose app
